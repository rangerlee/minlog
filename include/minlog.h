#ifndef MINLOG_H_INCLUDED
#define MINLOG_H_INCLUDED
/*
 * This file is part of the minlog and
 * licensed under the Apache License, version 2
 * http://www.apache.org/licenses/
 *
 * Copyright (c) 2015 rangerlee (rangerlee@foxmail.com)
 * Latest version available at: http://git.oschina.net/rangerlee/minlog.git
 *
 * This project is a simple logger.
 * More information can get from README.md
 *
 */

namespace minlog{

#define MINLOG_VERSION_MAJOR 0
#define MINLOG_VERSION_MINOR 3

/**
 * \brief 日志接口定义
 * \attention 未初始化之前调此接口目前有效，后期可能会变更
 */
struct log
{
    virtual void debug(const char* fmt, ...) = 0;         /**< 调试日志接口 */
    virtual void debug(const char* fmt, va_list& va) = 0; /**< 调试日志接口 */
    virtual void info(const char* fmt, ...) = 0;          /**< 信息日志接口 */
    virtual void info(const char* fmt, va_list& va) = 0;  /**< 信息日志接口 */
    virtual void warn(const char* fmt, ...) = 0;          /**< 警告日志接口 */
    virtual void warn(const char* fmt, va_list& va) = 0;  /**< 警告日志接口 */
    virtual void error(const char* fmt, ...) = 0;         /**< 错误日志接口 */
    virtual void error(const char* fmt, va_list& va) = 0; /**< 错误日志接口 */
    virtual void fatal(const char* fmt, ...) = 0;         /**< 严重日志接口 */
    virtual void fatal(const char* fmt, va_list& va) = 0; /**< 严重日志接口 */
};

/**
 * \brief minlog 初始化
 * \return int 返回0表示初始化成功，否则表示错误原因
 * \attention 当前版本不完善，后期此接口会变更
 */
int  init(const char* dir, int cache = 2);

/**
 * \brief 获取minlog全局实例
 * \return log* 返回全局实例，不会返回NULL
 */
log* instance();

/**
 * \brief 日志类型定义
 */
enum media{
    null,       /**< 无效值 */
    console = 10,    /**< 标准输出 */
    localfile = 20,  /**< 本地文件 */
};

/**
 * \brief 日志级别定义
 */
enum level{
    none_level,     /**< 无效值 */
    debug_level,    /**< 调试 */
    info_level,     /**< 信息 */
    warn_level,     /**< 警告 */
    error_level,    /**< 错误 */
    fatal_level,    /**< 严重 */
};

/**
 * \brief 设置日志类型的级别
 * \param media 需要设置的日志类型
 * \enum media
 * \param level 需要设置的日志级别
 * \enum level
 * \note 设置实时生效
 */
void setlevel(media, level);

/**
 * \brief 取消日志类型的级别
 * \param media 需要设置的日志类型
 * \enum media
 * \param level 需要设置的日志级别
 * \enum level
 * \note 设置实时生效
 */
void unsetlevel(media, level);

/**
 * \brief minlog 逆初始化
 */
void fini();

}
#endif
