#include <iostream>
#include <cstdio>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <cstdlib>
#include <unistd.h>
#include <stdarg.h>
using namespace std;

#include "../include/minlog.h"

void mylog(const char* fmt, ...)
{
    va_list marker;
    va_start(marker,fmt);
    minlog::instance()->debug(fmt,marker);
    va_end(marker);
}

int main()
{
    minlog::init("../logs");
    minlog::setlevel(minlog::console, minlog::debug_level);
    minlog::setlevel(minlog::console, minlog::info_level);
    minlog::setlevel(minlog::localfile, minlog::info_level);
	minlog::setcache(1);

    minlog::instance()->debug("hello %s", "world");
    minlog::instance()->info("hello %s", "world");
    minlog::instance()->info("hello %s", "world");
    mylog("hello %s!","world");
    mylog("hello %s!","world");
    minlog::fini();
    return 0;
}
