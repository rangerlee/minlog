minlog
============================================
a simple c\c++ log<br/>

---
### Build
目前仅支持Linux，编译依赖cmake工具
```shell
cmake . #-DCMAKE_BUILD_TYPE=Release[Debug,...]
make
```

### Changes
minlog v0.1 
- 采用线程安全的无锁队列进行消息传递
- 采用C语言标准格式化字符串
- 按小时命名文件及分割文件

minlog v0.2
- 修复内存泄漏问题
- 增加文件缓存方式可配

minlog v0.3
- 增加配置日志文件路径功能


### Demo
test目录，演示所有接口调用
```cpp
minlog::init("./logs");
minlog::setlevel(minlog::console, minlog::debug_level);
minlog::setlevel(minlog::console, minlog::info_level);
minlog::setlevel(minlog::localfile, minlog::info_level);

minlog::instance()->debug("hello %s", "world");
minlog::instance()->info("hello %s", "world");

minlog::fini();
```

编译
```shell
cd test
cmake . 
./test
```

### Task
Windows支持<br/>
支持配置(当前暂无配置，以小时分割文件)<br/>
支持稳定模式和急速模式配置(当前高并发导致内存占用高)<br/>
支持其他方式日志保存(如网络)<br/>


>OSC主页：[http://my.oschina.net/rangerlee](http://my.oschina.net/rangerlee)<br/>
>联系方式：[rangerlee@foxmail.com](mailto:rangerlee@foxmail.com)
